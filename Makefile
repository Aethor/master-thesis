build: clean temp-assets
	pdflatex --shell-escape thesis.tex
	bibtex thesis
	pdflatex --shell-escape thesis.tex
	pdflatex --shell-escape thesis.tex

.PHONY: temp-assets
temp-assets:
	perl get_temp_assets.pl

clean:
	rm thesis.{aux,bbl,blg,log,toc,lof,lot}; true

