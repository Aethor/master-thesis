$temp_assets_dir = "./temp-assets/";
%files = (
    "precision-vs-recall.png" => "http://cloud.klmp200.net/index.php/s/aHWiNsC7x7jBMpj/download",
    "attention-trained-vs-untrained.png" => "http://cloud.klmp200.net/index.php/s/fZc5Tg9CMoT2mQL/download",
    "visualizer.png" => "http://cloud.klmp200.net/index.php/s/3Sfk9yyKsE8yeow/download",
    "ncu.png" => "http://cloud.klmp200.net/index.php/s/Pg4njeSg5Ta7AQr/download"
);


for (keys %files) {

    $filename = $temp_assets_dir . $_;

    if (-f $filename) {
	print "$filename already exists\n";
	next;
    }

    $url = $files{$_};
    print "downloading $filename from $url\n";
    system "wget", "-q", $files{$_}, "-O", $filename;
}
