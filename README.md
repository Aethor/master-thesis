# Natural Language Processing applied to Interative Character Relationships Visualization in Novels

# Abstract

 Due to their importance in humanity culture, literary works have been extensively studied during the course of history. In those works, characters and their relationships often play a central role. The study of the structure of those relationships is the study of character networks : that is, a special kind of graph that can be used to represent these structures.

Due to the importance of dialogue between characters, one can extract a specialised kind of network : a conversational network, extracted using only dialogues between characters. Using tools from graph theory or other fields of computer science, those networks can be studied to reveal original insights unattainable from traditional literary analysis.

This work is dedicated to the automatic extraction of dynamic signed conversational networks. We propose a general method to extract those kinds of networks, that can be used in any type of work. We then show an example where we apply this method on novels in particular, which makes us propose a new technique for automatic utterance attribution. Lastly, we create a simple example of software allowing the visualization of extracted networks to analyze them.


# Generating 

If you have a pdflatex distribution, simply running `make` should take care of everything.


# Getting an already generated PDF version

The latest version should be available [here](https://cloud.klmp200.net/index.php/s/f7YRqcW9oAwwLKd).
